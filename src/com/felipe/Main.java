package com.felipe;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        /* numeros entero */
        int numero = 100;
        System.out.println(numero);

        int numeroF = 10/3;
        System.out.println(numeroF);

        //int numerA = 99816627783;

        /* numeros flotantes */

        float numberFloat = 10.1f;
        System.out.println(numberFloat);

        /* numeros Double */

        double numberDouble = 10.1;
        System.out.println(numberDouble);

        /* Cadenas or String */

        String cadena = "Felipe Carrasco";
        System.out.println(cadena);

        String cadenaN = "1234567890";
        System.out.println(cadenaN);

        String cadenaO = "$%&/()=";
        System.out.println(cadenaO);

        String cadenaP = "\"\"";
        System.out.println(cadenaP);

        /* Leyendo datos desde el teclado */
        System.out.println("Escribe numero");
        Scanner leer = new Scanner(System.in);
        int numeroScan = leer.nextInt();
        System.out.println(numeroScan);

        /* Operador suma */
        int numA, numB;
        numA = 9;
        numB = 10;

        int r = numA + numB;
        System.out.println(r);
        System.out.println(numA + numB);

        /* Operador resta */

        int a, b, c;
        a = 11;
        b = 15;
        c = a - b;
        System.out.println(c);

        /* Operador de multiplicación */

        int x, y, z;

        x = 2;
        y = 3;
        z = x * y;

        System.out.println(z);

        /* Operador de división */
        int aa, bb, cc;
        float e;
        aa = 20;
        bb = 3;
        cc = aa/bb;
        e = aa/bb;
        System.out.println(cc);
        System.out.println(e);

        /* Operador para concatenar */
        int f, g;
        f = 1;
        g = 1;

        String p = "2";
        String t = "1";
        System.out.println(p+t);

        System.out.println(f + g);

        /* Operador para incrementar y decrementar */
        int i;
        i = 0;
        int decremento = 1;
        i = i + 1;
        i++;
        decremento-=5;

        System.out.println(i);
        System.out.println(decremento);

        /* Operadores relacionales */
        // < menor que
        // > mayor que

        // 10 < 5 = false
        // 10 > 5 = true
        // 10 == 5 false
        // 10 == 10 true

        /* Operadores relacionales combinados */
        // 20 >= 20 true
        // 20 != 10 true

        /* Conociendo el ciclo For */

        for (int zz = 0; zz < 10; zz++){
            System.out.println(zz);
        }
        System.out.println("fuera");

        /* El ciclo While */

        int xx = 1;
        while(xx<20) {
            System.out.println(xx);
            xx++;
        }

        /* Presentamos el ciclo Do While */

        int yy = 1;
        do {
            System.out.println(yy);
            yy++;
        } while(yy < 50);

        /*El ciclo For Each*/
        String[] list = { "josé", "Felipe", "ana", "pedro"};
        for (String nombre: list) {
            System.out.println(nombre);
        }

        /* Hola en IF */
        int edad = 10;
        if(edad > 18) {
            // bloque 1
            System.out.println("bloque 1");
        }else {
            // bloque 2
            System.out.println("bloque 2");
        }

        /* Operadores lógicos en Java */

        int valor = 1;

        if(valor != 1) {
            System.out.println("Bloque 1");
        } else {
            System.out.println("Bloque 2");
        }

        /* IF anidados */
        int num = 9;
        int numeB = 5;

        if(num>0){
            if (num < 10) {
                System.out.println("Bloque 1");
                if (num>5){
                    System.out.println("Bloque 3");
                    if (numeB >19) {
                        System.out.println("Bloque 4");
                    }
                }
            }
        } else {
            System.out.println("Bloque 2");
        }

        /* Else IF */
        int exampleNum = 10;

        if(exampleNum == 9) {
            System.out.println("Es 9");
        } else if (exampleNum == 8) {
            System.out.println("Es 8");
        } else if (exampleNum == 10){
            System.out.println("Es 10");
        }

        /*Estructura básica de control Switch*/

        int numeroSwitch = 6;

        switch (numeroSwitch) {
            case 1:
                System.out.println("1");
                break;
            case 2:
                System.out.println("2");
                break;
            case 3:
                System.out.println("3");
                break;
            case 4:
                System.out.println("4");
                break;
            case 5:
                System.out.println("5");
                break;
            case 6:
                System.out.println("6");
                break;
            default:
                System.out.println("x");
                break;
        }

        /* La instrucción Try catch */

        try {
            int numCatch = 4/0;
        }catch (Exception error) {
            System.out.println(error.getMessage());
        }

        /* Creación de matrices */
        int[] numberList = new int[4];

        numberList[0] = 1;
        numberList[1] = 2;
        numberList[2] = 3;
        numberList[3] = 4;

        System.out.println(numberList[3]);

        /* Manejo de índices */
        int[] arreglo = new int[10];

        Scanner scanner = new Scanner(System.in);

        int index;

        for (index = 0; index < arreglo.length; index++){
            System.out.println("Poner dato: ");
            arreglo[index] = scanner.nextInt();
        }

        for (index = 0; index < arreglo.length; index++) {
            System.out.println(arreglo[index]);
        }

        /* Matriz bidimensional */

        int[] numerosBidimensional = new int[2];
        int[][] arregloBidimendional = new int[4][4];

        Scanner read = new Scanner(System.in);

        int ii, jj;

        for (ii = 0; ii < arregloBidimendional.length; ii++) {
            for (jj = 0; jj < arregloBidimendional.length; jj++) {
                System.out.println("Ingresa numero");
                arregloBidimendional[ii][jj] = read.nextInt();
            }
        }

        for (ii = 0; ii < arregloBidimendional.length; ii++) {
            for (jj = 0; jj < arregloBidimendional.length; jj++) {
                System.out.println(arregloBidimendional[ii][jj]);
            }
        }

    }

}
